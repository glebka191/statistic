import java.util
import java.util.Date

import org.apache.commons.lang.time.DateUtils
import org.apache.hadoop.fs._

class Folder (names : String, path : RemoteIterator[LocatedFileStatus], fs:FileSystem, fullPath:Path) {
  private var name = names
  private var files = new util.ArrayList[Double]
  private var subfolderSize = 0

  private var modificationDate = new util.ArrayList[Date]()

  while (path.hasNext) {
    val file = path.next()
    if (!file.getPath.getName.endsWith("_SUCCESS") && file.getLen!=0) {
      files.add(file.getLen)
      var newDate = file.getModificationTime
      var date = new Date(newDate)
      modificationDate.add(date)
    }
  }



  var paths = fs.listStatus(new Path(fullPath + "/" + getName()))
  getCountPath(paths)

  def getCountPath(paths:Array[FileStatus]): Unit ={
    /*
    paths.foreach(x => if(x.isDirectory == true){
      val z = fs.listStatus(x.getPath)
      subfolderSize+=1
      getCountPath(z)
    })
     */
    var i = 0
    while (i < paths.size){
      if (paths(i).isDirectory == true){
        val z = fs.listStatus(paths(i).getPath)
        subfolderSize+=1
        getCountPath(z)
      }
      i+=1
    }
  }

  def getFiles(): util.ArrayList[Double] ={
    files
  }


  def getFolder(): Int ={
    subfolderSize
  }

  def setFiles(files : util.ArrayList[Double]): Unit ={
    this.files = files
  }

  def getName(): String ={
    name
  }

  def setName(name : String): Unit ={
    this.name = name
  }

  def max(): Double ={
    var max = 0.0
    var i = 0
    //files.forEach(x => if (x > max) max = x)
    while (i < files.size()){
      if (files.get(i) > max) max = files.get(i)
      i+=1
    }
    max/(1024*1024)
  }

  def min(): Double ={
    if (files.size() == 0){
      return 0.0
    }
    var min = files.get(0)
    var i = 0
    //files.forEach(x => if (x < min) min = x)
    while (i < files.size()){
      if (files.get(i) < min) min = files.get(i)
      i+=1
    }
    min/(1024*1024)
  }

  def avrSize(): Double ={
    var sum = 0.0
    var i = 0
    if (files.size() == 0){
      return 0.0
    }
    while (i<files.size()){
      sum += files.get(i)
    i+=1
    }
    //files.forEach(x => sum += x)
    val result = sum/files.size()
    result/(1024*1024)
  }

  def getCountBelow10: Int ={
    var i = 0
    var x = 0
    /*
    getFiles().forEach(x=>
      if (x < 10485760){
        i+=1
      })
     */
    while (i < getFiles().size()){
      if (getFiles().get(i) < 10485760) x+=1
      i+=1
    }
    x
  }

  def getCountBelow50: Int ={
    var i = 0
    var x = 0
    /*
    getFiles().forEach(x=>
      if (x > 10485760 && x < 52428800){
        i+=1
      })
     */
    while (i < getFiles().size()){
      if (getFiles().get(i) > 10485760 && getFiles().get(i) < 52428800) x+=1
      i+=1
    }

    x
  }

  def getCountMore50: Int ={
    var i = 0
    var x = 0
    /*
    getFiles().forEach(x=>
      if (x > 52428800){
        i+=1
      })
     */
    while (i < getFiles().size()){
      if (getFiles().get(i) > 52428800) x+=1
    i+=1
    }
    x
  }

  def maxDateChange: Date ={
    if (modificationDate.size() == 0){
      return null
    }
    var i = 0
    var dt = modificationDate.get(0)
    /*
    modificationDate.forEach(x => if (x.after(dt)){
      dt = x
    })
     */
    while(i < modificationDate.size()){
      if (modificationDate.get(i).after(dt)) dt = modificationDate.get(i)
    i+=1
    }
    dt
  }

  def minDateChange: Date ={
    if (modificationDate.size() == 0){
      return null
    }
    var dt = modificationDate.get(0)
    var i = 0
    /*
    modificationDate.forEach(x => if (x.before(dt)){
      dt = x
    })
     */
    while (i < modificationDate.size()){
      if (modificationDate.get(i).before(dt)) dt = modificationDate.get(i)
      i+=1
    }
    dt
  }

  def countFiles: Int ={
    var i = 0
    var x = 0
    var dateBefor5 = DateUtils.addDays(new Date, -5)
    if (modificationDate.size() == 0){
      return 0
    }else {
      while (i < modificationDate.size()){
        if (dateBefor5.before(modificationDate.get(i))) x+=1
        i+=1
      }
    }
    x
  }
}

//      modificationDate.forEach(x =>
//        if (dateBefor5.before(x)) {
//          i += 1
//        }