import org.apache.spark.sql.{DataFrame, SaveMode}

class WriteToHDFS(hdfs:String) {
  def write(frame:DataFrame): Unit ={
    frame.repartition(1)
      .write.format("csv").mode(SaveMode.Overwrite).csv("hdfs://" + hdfs + "/tmp/tmp_statisitc/")
  }
}
