import java.text.{DecimalFormat, SimpleDateFormat}
import java.util
import java.util.Date

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import scala.collection.mutable.ArrayBuffer

class ReadFS(hdfs:String, paths:String) {
  var size = 0
  val conf = new Configuration()
  System.setProperty("HADOOP_USER_NAME", "hdfs")
  val path = new Path(paths)
  conf.set("fs.default.name", hdfs)

  val fs = FileSystem.get(conf)
  var allFolders = new ArrayBuffer[Folder]
  var allFiles = new util.ArrayList[Double]()
  var allFL = new util.ArrayList[Int]()
  val listDF = new util.ArrayList[DataFrame]()
  var allsbDir = 0

  val allMaxDates = new util.ArrayList[Date]()
  val allMinDates = new util.ArrayList[Date]()

  val format = new SimpleDateFormat("dd/MM/yy HH:mm:ss")

  def fun() {
    val getAllFiles = fs.listStatus(path)

    var l = 0
    while (l < getAllFiles.length){
      allFolders+=(new Folder(getAllFiles(l).getPath.getName, fs.listFiles(getAllFiles(l).getPath, true), fs, path))
      l+=1
    }


    //getAllFiles.foreach(x => allFolders+=(new Folder(x.getPath.getName, fs.listFiles(x.getPath,true), fs, path)))

    //allFolders.foreach(x => x.getFiles().forEach(e => allFiles.add(e)))

    var a = 0
    while (a < allFolders.size){
      var i = 0
      while (i < allFolders(a).getFiles().size()){
        allFiles.add(allFolders(a).getFiles().get(i))
        i+=1
      }
      a+=1
    }

    //allFolders.forEach(x => allFL.add(x.getFolder()))

    var b = 0
    while (b < allFolders.size) {
      allFL.add(allFolders(b).getFolder())
      b+=1
    }


    /*
    allFolders.forEach(x => if(x.maxDateChange!=null){
      allMaxDates.add(x.maxDateChange)
    })
     */
    var c = 0
    while (c < allFolders.size){
      if (allFolders(c).maxDateChange!=null){
        allMaxDates.add(allFolders(c).maxDateChange)
      }
      c+=1
    }


    /*
    allFolders.forEach(x => if (x.minDateChange!=null){
      allMinDates.add(x.minDateChange)
    })
     */
    var d = 0
    while (d < allFolders.size){
      if (allFolders(d).minDateChange!=null) allMinDates.add(allFolders(d).minDateChange)
      d+=1
    }

    val spark = SparkSession.builder().master("local").getOrCreate()
    val sc = spark.sparkContext

    val schema = StructType(Seq(
      StructField("Table", StringType,  true),
      StructField(" CountFiles ", IntegerType, true),
      StructField(" subDir ", IntegerType, true),
      StructField("AVG_MB", StringType, true),
      StructField("MAX_MB ", StringType, true),
      StructField("MIN_MB ", StringType, true),
      StructField(" Less_10MB ", IntegerType, true),
      StructField(" Between 10-50MB ", IntegerType, true),
      StructField(" More_50MB ", IntegerType, true),
      StructField(" Max-Date", StringType, true),
      StructField(" Min-Date", StringType, true),
      StructField("Last 5 days", IntegerType, true)
    ))

    var e = 0
    while (e < allFolders.size){
      allsbDir+=allFolders(e).getFolder()
      e+=1
    }
    //var allSubDir = allFolders.forEach(x => allsbDir += x.getFolder())
    val avgAll = new DecimalFormat("#0.000").format(avr(allFiles))
    val maxe = new DecimalFormat("#0.000").format(max(allFiles))
    val minx = new DecimalFormat("#0.000").format(min)
    val myDf = Row("SUMMARY", allFiles.size(), allsbDir, avgAll, maxe,
      minx, getCountBelow10(allFiles), getCountBelow50(allFiles), getCountMore50(allFiles),
      format.format(getMaxDateFileChange),
      format.format(minDateChange), summary)
    val r = sc.parallelize(Seq(myDf))
    val ddf = spark.createDataFrame(r, schema)

    var emptyDF = spark.createDataFrame(spark.sparkContext.emptyRDD[Row],
      schema)

    var i = 0
    while (i < allFolders.size){
      val avg = new DecimalFormat("#0.000").format(allFolders(i).avrSize())
      val maxe = new DecimalFormat("#0.000").format(allFolders(i).max())
      val mine = new DecimalFormat("#0.000").format(allFolders(i).min())
      def mx(): String ={
        if (allFolders(i).maxDateChange == null){
          null
        }else{
          format.format(allFolders(i).maxDateChange)
        }
      }
      def mi(): String ={
        if (allFolders(i).minDateChange == null){
          null
        }else{
          format.format(allFolders(i).minDateChange)
        }
      }
      val abc = Row(allFolders(i).getName(), allFolders(i).getFiles().size(),
        allFolders(i).getFolder(), avg,
        maxe, mine, allFolders(i).getCountBelow10,
        allFolders(i).getCountBelow50, allFolders(i).getCountMore50,
        mx(),
        mi(),
        allFolders(i).countFiles)

      val row = sc.parallelize(Seq(abc))
      val df = spark.createDataFrame(row, schema)
      val newDf = emptyDF.union(df)
      emptyDF = newDf
      i+=1
    }
    val dd2 = emptyDF.union(ddf)

   // var write = new WriteToHDFS(hdfs).write(dd2)
    dd2.show()
  }



  def max(allFiles:util.ArrayList[Double]): Double ={
    var max = 0.0
    var i = 0
    //allFiles.forEach(x => if (x > max) max = x)
    while (i < allFiles.size()){
      if (allFiles.get(i) > max) max = allFiles.get(i)
      i+=1
    }
    max/(1024*1024)
  }

  def min(): Double ={
    if (allFiles.size() == 0){
      return 0.0
    }
    var min = allFiles.get(0)
    var i = 0
    //allFiles.forEach(x => if (x < min) min = x)
    while (i < allFiles.size()){
      if (allFiles.get(i) < min) min = allFiles.get(i)
      i+=1
    }
    min/(1024*1024)
  }

  def avr(allFiles:util.ArrayList[Double]): Double ={
    var sum = 0.0
    var i = 0
    //allFiles.forEach(x => sum += x)
    while (i < allFiles.size()){
      sum+=allFiles.get(i)
      i+=1
    }
    val result = sum/allFiles.size()
    result/(1024*1024)
  }

  def sum(allFl:util.ArrayList[Int]): Double ={
    var sum = 0.0
    var i = 0
    //allFl.forEach(x => sum += x)
    while (i < allFiles.size()){
      sum+=allFiles.get(i)
      i+=1
    }
    sum
  }

  def getCountBelow10(allFl:util.ArrayList[Double]): Int ={
    var i = 0
    var x = 0
    /*
    allFl.forEach(x=>
      if (x < 10485760){
        i+=1
      })
     */
    while (i < allFl.size()){
      if (allFl.get(i)< 10485760) x+=1
    i+=1
    }
    x
  }

  def getCountBelow50(allFl:util.ArrayList[Double]): Int ={
    var i = 0
    var x = 0
    /*
    allFl.forEach(x=>
      if (x > 10485760 && x < 52428800){
        i+=1
      })
     */
    while (i < allFl.size()){
      if (allFl.get(i) > 10485760 && allFl.get(i) < 52428800) x+=1
      i+=1
    }
    x
  }

  def getCountMore50(allFl:util.ArrayList[Double]): Int ={
    var i = 0
    var x = 0
    /*
    allFl.forEach(x=>
      if (x > 52428800){
        i+=1
      })
     */
    while (i < allFl.size()){
      if (allFl.get(i) > 52428800) x+=1
    i+=1
    }
    x
  }

  def getMaxDateFileChange: Date ={
    if (allMaxDates.size() == 0){
      return null
    }
    var maxDt = allMaxDates.get(0)
    var i = 0
    /*
    allMaxDates.forEach(x=>if (x.after(maxDt)){
      maxDt = x
    })
     */
    while (i < allMaxDates.size()){
      if (allMaxDates.get(i).after(maxDt)) maxDt = allMaxDates.get(i)
      i+=1
    }
    maxDt
  }

  def minDateChange: Date ={
    if (allMinDates.size() == 0){
      return null
    }
    var dt = allMinDates.get(0)
    var i = 0
    /*
    modificationDate.forEach(x => if (x.before(dt)){
      dt = x
    })
     */
    while (i < allMinDates.size()){
      if (allMinDates.get(i).before(dt)) dt = allMinDates.get(i)
      i+=1
    }
    dt
  }

    /*
    allMinDates.forEach(x=>if (x.before(minDt)){
      minDt = x
    })
     */

  def summary: Int ={
    var x = 0
    var i = 0
    //allFolders.forEach(x => i+=x.countFiles)
    while (i < allFolders.size){
      x+=allFolders(i).countFiles
      i+=1
    }
    x
  }
}